import torch
import os
import sys
import platform
import torch.nn.functional as f
import numpy as np
import matplotlib.pyplot as plt

from src.data_handlers import SiameseNetworkDataset
from torch.utils.data import DataLoader
from sklearn.metrics import precision_recall_curve, auc, confusion_matrix, accuracy_score, precision_score, \
    recall_score, roc_curve, fbeta_score
from typing import List


# define constants
NUM_WORKERS = 0 if platform.system() == "Windows" else 2
IS_CUDA_CAPABLE = torch.cuda.is_available()

if len(sys.argv) < 2:
    print("Please provide the path to the model.")
    sys.exit(1)

MODEL_PATH = sys.argv[1]
DATASET_NAME = "SigComp2011" if len(sys.argv) < 3 else sys.argv[2]
ALTERNATIVE_METRIC = len(sys.argv) > 3 and sys.argv[3] == "1"


def draw_precision_recall_curve(precision: List[float], recall: List[float], title: str) -> None:
    """
    Draw a plot of the Precision-Recall Curve and computes the area under it.

    @param precision: List[float]
    @param recall: List[float]
    @param title: str, title for the plot
    """

    au_prc = auc(recall, precision)
    
    plt.plot(recall, precision, label="Area under the curve: {:.3f}".format(au_prc))
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title(title)

    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)

    plt.legend()
    plt.show()


def load_and_evaluate() -> None:
    """
    Load the saved model, evaluate and print its performance based on the chosen metric
    specified in the program's arguments.
    Exits the program if the arguments from the command line are invalid.
    """
    
    try:
        # Load the model
        try:
            if not IS_CUDA_CAPABLE:
                model = torch.load(MODEL_PATH, map_location=torch.device('cpu'))

            else:
                model = torch.load(MODEL_PATH)
        except Exception as e:
            print("The model path is invalid.")
            sys.exit(1)
        
        # Create the dataset and dataloader for testing
        try:
            dataset_path = os.path.join("dataset", DATASET_NAME)
            test_dataset = SiameseNetworkDataset(dataset_path, False, 128)
        except Exception:
            print("An error while loading the dataset occurred. Check that {} exists".format(DATASET_NAME))
            sys.exit(1)
        
        test_dataloader = DataLoader(test_dataset, shuffle=True, num_workers=NUM_WORKERS, batch_size=1)
    
        # Evaluate and print the performance of the model
        predicted = []
        actual = []
        
        # Evaluate each sample from the dataset
        for data in test_dataloader:
            image1, image2, label = data
            
            # Transfer to the computation device
            if IS_CUDA_CAPABLE:
                image1, image2 = image1.cuda(), image2.cuda()
            else:
                image1, image2 = image1.cpu(), image2.cpu()

            # Feed the network the sample and get outputs
            output1, output2 = model(image1, image2)
            
            # Calculate the pairwise distance of outputs, returns single value
            distance = f.pairwise_distance(output1, output2).item()
            
            # Collect the predictions and the ground truth
            if distance is not None:                    
                predicted.append(distance)
                actual.append(label.item())
                
        # Perform normalization so that all predictions fit into range [0, 1]        
        if max(predicted) == 0:
            print("The model did not find any meaningful predictions.")
            sys.exit(1)
        
        predicted = [float(prediction) / max(predicted) for prediction in predicted]
                
        # Find the threshold
        threshold = get_threshold(predicted, actual, ALTERNATIVE_METRIC)
        
        # Assign classes to predictions based on the threshold
        predicted_thresholded = list(
            map(lambda prediction: 0 if prediction < threshold else 1, predicted))

        # Calculate Precision-Recall Curve and the area under it
        precision, recall, _ = precision_recall_curve(actual, predicted)
        au_prc = auc(recall, precision)

        # Construct confusion matrix, compute all metrics
        tn, fp, fn, tp = confusion_matrix(actual, predicted_thresholded).ravel()
        
        accuracy_value = accuracy_score(actual, predicted_thresholded)
        recall_value = recall_score(actual, predicted_thresholded)
        precision_value = precision_score(actual, predicted_thresholded)
        
        f2 = fbeta_score(actual, predicted_thresholded, beta=2, zero_division=0)
        f1 = fbeta_score(actual, predicted_thresholded, beta=1, zero_division=0)
        f0_5 = fbeta_score(actual, predicted_thresholded, beta=0.5, zero_division=0)

        print("AU PRC: {}, F2-score: {}, F1-score: {}, F0.5-score: {}".format(au_prc, f2, f1, f0_5))
        print("TN: {}, FP: {}, FN: {}, TP: {}".format(tn, fp, fn, tp))
        print("Accuracy: {}, Precision: {}, Recall: {}".format(accuracy_value, precision_value, recall_value))
        
    except Exception:
        print("An exception occurred during the execution.")
        sys.exit(1)


def get_threshold(predicted: List[float], actual: List[int], alternative_metric: bool) -> float:
    """
    Find the optimal threshold that yields the highest F2-score (alternate_metric: False) 
    or the highest accuracy (alternate_metric: True)
    
    @param predicted: List[float], the predictions made by the model
    @param actual: List[int], the ground truth of the predictions
    @param alternative_metric: bool, signifies what metric to use (highest F2-score or highest accuracy)
    
    @return: float, the threshold
    
    """
    
    if alternative_metric:
        # Compute accuracies for all thresholds
        _, _, thresholds = roc_curve(actual, predicted)

        accuracy_scores = []
        for threshold in thresholds:
            accuracy_scores.append(accuracy_score(actual, [prediction > threshold for prediction in predicted]))
        
        accuracies = np.array(accuracy_scores)
        
        # Find the threshold that yields the highest accuracy
        return thresholds[accuracies.argmax()]
                
    else:
        # Calculate Precision-Recall Curve
        precision, recall, thresholds = precision_recall_curve(actual, predicted)

        # Calculate F2-scores for all thresholds
        fscore = (5 * precision * recall) / (4 * precision + recall)
        # If division by zero occurred on some elements, zero-out the result
        fscore[fscore == np.inf] = 0

        # Find the threshold that yields the highest F2-score
        return thresholds[np.argmax(fscore)]

    
if __name__ == '__main__':
    load_and_evaluate()
