import platform
import sys
import os
import numpy as np

from src.data_handlers import SiameseNetworkDataset
from sklearn.dummy import DummyClassifier
from sklearn.metrics import precision_recall_curve, auc, confusion_matrix, accuracy_score, recall_score
from sklearn.metrics import precision_score, fbeta_score
from torch.utils.data import DataLoader


# Define constants
NUM_WORKERS = 0 if platform.system() == "Windows" else 2
DATASET_NAME = "SigComp2011" if len(sys.argv) < 2 else sys.argv[1]


def train_and_evaluate() -> None:
    """
    "Trains" and computes the Area under Precision-Recall Curve (AU PRC) of a dummy model.
    """
    
    train_X = []
    train_y = []
    
    test_X = []
    test_y = []

    # Initialize datasets and dataloaders
    dataset_path = os.path.join("dataset", DATASET_NAME)
    
    try:
        train_dataset = SiameseNetworkDataset(dataset_path, True, 1)
        train_dataloader = DataLoader(train_dataset, 1, shuffle=False, num_workers=NUM_WORKERS)

        test_dataset = SiameseNetworkDataset(dataset_path, False, 1)
        test_dataloader = DataLoader(test_dataset, 1, shuffle=False, num_workers=NUM_WORKERS)
        
    except Exception:
        print("An error while loading the dataset occurred. "
              "Check that {} and its index file exists".format(DATASET_NAME))
        sys.exit(1)
    
    # Iterate through the data and collect labels
    for data in train_dataloader:
        _, _, label = data

        # Input is ignored
        train_X.append(0)
        
        train_y.append(label.item())

    for data in test_dataloader:
        _, _, label = data

        # Input is ignored
        test_X.append(0)
        
        test_y.append(label.item())

    # Create DummyClassifier and "train" it
    dummy_classifier = DummyClassifier(strategy="prior")
    dummy_classifier.fit(test_X, test_y)
    
    # Get predictions
    predictions = dummy_classifier.predict_proba(test_X)[:, 1]
    
    # Evaluate the same way as in the evaluation of a normal model 
    # Calculate Precision-Recall Curve
    precision, recall, thresholds = precision_recall_curve(test_y, predictions)

    # Calculate F2-scores for all thresholds
    fscore = (5 * precision * recall) / (4 * precision + recall)
    # If division by zero occurred on some elements, zero-out the result
    fscore[fscore == np.inf] = 0

    # Find the threshold that yields the highest F2-score
    threshold = thresholds[np.argmax(fscore)]
    
    # Assign classes to predictions based on the threshold
    predicted_thresholded = list(
        map(lambda prediction: 0 if prediction < threshold else 1, predictions))

    # Calculate AU PRC
    au_prc = auc(recall, precision)

    # Construct confusion matrix, compute all metrics
    tn, fp, fn, tp = confusion_matrix(test_y, predicted_thresholded).ravel()
    
    accuracy_value = accuracy_score(test_y, predicted_thresholded)
    recall_value = recall_score(test_y, predicted_thresholded)
    precision_value = precision_score(test_y, predicted_thresholded)
    
    f2 = fbeta_score(test_y, predicted_thresholded, beta=2, zero_division=0)
    f1 = fbeta_score(test_y, predicted_thresholded, beta=1, zero_division=0)
    f0_5 = fbeta_score(test_y, predicted_thresholded, beta=0.5, zero_division=0)

    print("AU PRC: {}, F2-score: {}, F1-score: {}, F0.5-score: {}".format(au_prc, f2, f1, f0_5))
    print("TN: {}, FP: {}, FN: {}, TP: {}".format(tn, fp, fn, tp))
    print("Accuracy: {}, Precision: {}, Recall: {}".format(accuracy_value, precision_value, recall_value))


if __name__ == '__main__':
    train_and_evaluate()
