import torch
import optuna
import platform
import sys
import random
import string
import os

from optuna import Trial
from torch.utils.data import DataLoader
from src.data_handlers import SiameseNetworkDataset
from src.net import SiameseNetwork, ContrastiveLoss
from src.train import train
from src.suggestions import suggest_params_general
from src.builders import build_layers
from src.evaluation import evaluate


# Defining program constants
NUM_WORKERS = 0 if platform.system() == "Windows" else 2
IS_CUDA_CAPABLE = torch.cuda.is_available()
MAX_SECONDS_PER_TRAIN = 7200 if len(sys.argv) < 2 else int(sys.argv[1])

# Directory where trained models will be saved
MODEL_DIR = "." if len(sys.argv) < 3 else sys.argv[2]
DATASET_NAME = "SigComp2011" if len(sys.argv) < 4 else sys.argv[3]


def objective(trial: Trial) -> float:
    """
    Perform one trial of the study. Saves the trained model to the directory specified in the program arguments.
    
    @param trial: Trial
    
    @return float, area under Precision-Recall Curve, or 0 if the trial failed
    """
    
    try:
        # Get suggestions
        suggest_params_general(trial)
        
        # Build the layers and the network
        c_layers_built, f_layers_built = build_layers(trial)
        net = SiameseNetwork(c_layers_built, f_layers_built)
        
        # Build the dataset and dataloader for training
        dataset_path = os.path.join("dataset", DATASET_NAME)
        
        try:
            train_dataset = SiameseNetworkDataset(dataset_path, True, 128)
        except Exception:
            print("An error while loading dataset occurred. Check that {} exists".format(DATASET_NAME))
            sys.exit(1)
            
        train_dataloader = DataLoader(train_dataset, shuffle=True, num_workers=NUM_WORKERS, 
                                      batch_size=trial.params["batch_size"])
        
        # Build the optimizer
        optimizer = torch.optim.Adam(net.parameters(), lr=trial.params["lr"], 
                                     betas=(trial.params["beta1"], trial.params["beta2"]),
                                     weight_decay=trial.params["weight_decay"])
        
        # Build the loss function
        loss_function = ContrastiveLoss(trial.params["margin"])
        
        # Train the model
        model = train(net, train_dataloader, optimizer, loss_function, trial.params["epochs"], 
                      IS_CUDA_CAPABLE, MAX_SECONDS_PER_TRAIN)

        # Render the current trial invalid if training failed
        if model is None:
            return 0.0

        # Build the dataloader for testing
        test_dataset = SiameseNetworkDataset(dataset_path, False, 128)
        test_dataloader = DataLoader(test_dataset, shuffle=True, num_workers=NUM_WORKERS, 
                                     batch_size=1)

        # Evaluate the model
        au_prc, _, _, _, _, _ = evaluate(net, test_dataloader, IS_CUDA_CAPABLE)

        # Generate random name
        model_name = "model_" + ''.join(random.choice(string.ascii_lowercase) for _ in range(16)) + ".pt"
        
        # Save the model
        torch.save(model, os.path.join(MODEL_DIR, model_name))
             
        print("Model with AU PRC of {} successfully saved as {}".format(au_prc, model_name))
             
        return au_prc

    except (RuntimeError, ValueError) as e:
        print(e)
        return 0.0


###############################################################################


if __name__ == '__main__':
    study = optuna.create_study(direction="maximize",
                                sampler=optuna.samplers.TPESampler())

    study.optimize(objective)
