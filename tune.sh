#!/bin/bash
#PBS -N signature-verification
#PBS -l select=1:ncpus=2:mem=32gb:scratch_local=16gb:ngpus=1:gpu_mem=24gb:gpu_cap=cuda61:cuda_version=12.1:cluster=galdor
#PBS -q gpu
#PBS -l walltime=24:00:00
#PBS -m n

# description of the comments above:
#   1. line: sets the name of the job
#   2. line: declares how many chunks, what resources and clusters the job will use
#   3. line: selects the queue
#   4. line: declares time limit for the job
#   5. line: sets what e-mail notifications will be sent


# declare where the project is saved
DATADIR=$HOME/signature-verification
# overriding the directory where the metadata of package installations are stored.
# otherwise, the installations will fail.
mkdir -p $DATADIR/tmp_install
TMPDIR=$DATADIR/tmp_install

# load pip module
module add py-pip/21.3.1-gcc-10.2.1-mjt74tn

# export python and python packages directory to environment variables
export PYTHONUSERBASE=$HOME/pyp_installs/cvmfs/software.metacentrum.cz/spack18/software/linux-debian11-x86_64_v2/gcc-10.2.1/python-3.9.12-rg2lpmkxpcq423gx5gmedbyam7eibwtc/cvmfs/software.metacentrum.cz/spack18/software/linux-debian11-x86_64_v2/gcc-10.2.1/python-3.9.12-rg2lpmkxpcq423gx5gmedbyam7eibwtc
export PATH=$PYTHONUSERBASE/bin:$PATH
export PYTHONPATH=$PYTHONUSERBASE/lib/python3.9/site-packages:$PYTHONPATH

# install necessary packages
pip3 install -q torch torchvision --root $HOME/pyp_installs/cvmfs/software.metacentrum.cz/spack18/software/linux-debian11-x86_64_v2/gcc-10.2.1/python-3.9.12-rg2lpmkxpcq423gx5gmedbyam7eibwtc
pip3 install -q optuna --root $HOME/pyp_installs/cvmfs/software.metacentrum.cz/spack18/software/linux-debian11-x86_64_v2/gcc-10.2.1/python-3.9.12-rg2lpmkxpcq423gx5gmedbyam7eibwtc
pip3 install -q scikit-learn --root $HOME/pyp_installs/cvmfs/software.metacentrum.cz/spack18/software/linux-debian11-x86_64_v2/gcc-10.2.1/python-3.9.12-rg2lpmkxpcq423gx5gmedbyam7eibwtc

# create a directory in the scratch directory and copy the project files there
mkdir $SCRATCHDIR/signature-verification
cp -a $DATADIR/dataset $SCRATCHDIR/signature-verification || { echo >&2 "Copying contents to scrach failed"; }
cp -a $DATADIR/src $SCRATCHDIR/signature-verification || { echo >&2 "Copying contents to scrach failed"; }
cp $DATADIR/tune.py $SCRATCHDIR/signature-verification || { echo >&2 "Copying contents to scratch failed"; }

# go to the copied project files
cd $SCRATCHDIR/signature-verification

# run the tuning
python tune.py 7200 $DATADIR SigComp2011 || { echo >&2 "Training failed. Code: $?"; exit 1; }
