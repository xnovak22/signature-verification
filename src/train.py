from typing import Optional
from torch.optim import Adam
from torch.utils.data import DataLoader
from .net import ContrastiveLoss, SiameseNetwork

import time


def train(net: SiameseNetwork, dataloader: DataLoader, optimizer: Adam, loss_function: ContrastiveLoss, epochs: int, 
          is_cuda_capable: bool, max_seconds_for_training: int) -> Optional[SiameseNetwork]:
    """
    Train the network.
    
    @param net: SiameseNetwork
    @param dataloader: DataLoader
    @param optimizer: Adam
    @param loss_function: ContrastiveLoss
    @param epochs: int
    @param is_cuda_capable: bool
    @param max_seconds_for_training: int
    
    @return the trained network, or None if exception occurred or training exceeded time limit
    """

    try:
        # Transfer the network to the computation device
        if is_cuda_capable:
            net = net.cuda()
        else:
            net = net.cpu()

        # Time when the training should be done
        max_finish = time.time() + max_seconds_for_training

        for epoch in range(0, epochs):
            # Check if the training exceeded the time limit
            if max_finish < time.time():
                return None

            for i, data in enumerate(dataloader, 0):
                img1, img2, label = data
                
                # Transfer the sample to the computation device
                if is_cuda_capable:
                    img1, img2, label = img1.cuda(), img2.cuda(), label.cuda()
                else:
                    img1, img2, label = img1.cpu(), img2.cpu(), label.cpu()

                # Zero-out gradients
                optimizer.zero_grad()

                # Feed the sample to the network
                output1, output2 = net(img1, img2)

                # Compute the loss
                loss_contrastive = loss_function(output1, output2, label)
                
                # Perform backward propagation
                loss_contrastive.backward()

                # Update parameters
                optimizer.step()
                
        return net

    except (RuntimeError, ValueError) as e:
        print(e)
        return None
