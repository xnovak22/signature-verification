import torch.nn.functional as f
import numpy as np

from .net import SiameseNetwork
from typing import Tuple
from sklearn.metrics import precision_recall_curve, auc, confusion_matrix, accuracy_score, fbeta_score
from torch.utils.data import DataLoader


# Suppress warnings about division by zero, it is taken care of
np.seterr(divide='ignore', invalid='ignore')

# Type declaration
Confusion_matrix = Tuple[int, int, int, int]


def evaluate(model: SiameseNetwork, dataloader: DataLoader, is_cuda_capable: bool) \
        -> Tuple[float, float, float, float, Confusion_matrix, Tuple[float, float, float]]:
    """
    Evaluates the model.

    @param model: SiameseNetwork
    @param dataloader: DataLoader
    @param is_cuda_capable: bool, indicates whether to use CUDA

    @return: Tuple[float, float, float, float, Confusion_matrix, Tuple[float, float, float]], AU PRC, F2, F1
    and F0.5 score, confusion matrix, precision, recall and accuracy. Throws ValueError exception
    if all predictions are 0.
    """

    # List used to store outputs on the test data
    predicted = []
    actual = []
        
    # Evaluate each sample from the dataset
    for data in dataloader:
        image1, image2, label = data
        
        # Transfer to the computation device
        if is_cuda_capable:
            image1, image2 = image1.cuda(), image2.cuda()
        else:
            image1, image2 = image1.cpu(), image2.cpu()

        # Feed the network the sample and get outputs
        output1, output2 = model(image1, image2)
        
        # Calculate the pairwise distance of the outputs, returns single value
        distance = f.pairwise_distance(output1, output2).item()

        # Collect the predictions and the true label
        if distance is not None:
            predicted.append(distance)
            actual.append(label.item())

    # Perform normalization so that all predictions fit into range [0, 1]
    if max(predicted) == 0:
        raise ValueError
    
    predicted = [float(prediction) / max(predicted) for prediction in predicted]

    # Calculate Precision-Recall Curve and the area under it
    precision, recall, thresholds = precision_recall_curve(actual, predicted)
    au_prc = auc(recall, precision)

    # Calculate F2-scores for all thresholds
    fscore = (5 * precision * recall) / (4 * precision + recall)
    # If division by zero occurred on some elements, zero-out the result
    fscore[fscore == np.inf] = 0

    # Find the threshold that yields highest F2-score
    best_fscore_index = np.argmax(fscore)

    # Find precision, recall
    recall_value = round(recall[best_fscore_index], ndigits=4)
    precision_value = round(precision[best_fscore_index], ndigits=4)

    # Assign classes to predictions based on the threshold
    optimal_threshold = thresholds[best_fscore_index]
    predicted_thresholded = list(
        map(lambda prediction: 0 if prediction < optimal_threshold else 1, predicted))

    # Construct confusion matrix, find F scores and accuracy
    tn, fp, fn, tp = confusion_matrix(actual, predicted_thresholded).ravel()
    accuracy_value = accuracy_score(
        actual, predicted_thresholded)
    f2 = np.max(fscore)
    f1 = fbeta_score(actual, predicted_thresholded, beta=1, zero_division=0)
    f0_5 = fbeta_score(actual, predicted_thresholded, beta=0.5, zero_division=0)

    return au_prc, f2, f1, f0_5, (tn, fp, fn, tp), (accuracy_value, precision_value, recall_value)
