from typing import Tuple
from optuna import Trial

import torch
import torch.nn as nn


def build_layers(trial: Trial) -> Tuple[nn.Sequential, nn.Sequential]:
    """
        Returns built convolutional and fully-connected layers based of the trial parameters.

        @param trial: Trial

        @return: Tuple[nn.Sequential, nn.Sequential], built convolutional and fully-connected layers
        """

    # Build convolutional layers
    c_layers_built = build_c_layers(trial)

    # Compute input size of the first fully-connected layer
    in1, in2, in3, in4 = c_layers_built(
        torch.empty(1, 1, 128, 128)).size()

    # Build fully-connected layers
    l_layers_built = build_l_layers(trial, in1 * in2 * in3 * in4)

    return c_layers_built, l_layers_built


def build_c_layers(trial: Trial) -> nn.Sequential:
    """
    Builds convolutional layers based on the trial parameters.

    @param trial: Trial

    @return: nn.Sequential, built convolutional layers
    """

    c_layers = []

    # Build convolutional layers (convolutions, maxpool, ReLU, dropout)
    for i in range(trial.params["c_layers"]):
        # Create key prefixes that will be used to fetch the trial parameters
        curr_name_prefix = "c_layers_{}_layer_{}".format(trial.params["c_layers"], i + 1)
        prev_name_prefix = "c_layers_{}_layer_{}".format(trial.params["c_layers"], i)

        # If the first convolutional layer is currently being built, set its input size to 1 
        # (the image passed to the network is grayscale - has one channel)
        input_size = 1 if i == 0 else trial.params["{}_output".format(prev_name_prefix)]

        # Reject trials which would build a network that is gradually shrinking its convolutional layers
        if input_size >= trial.params["{}_output".format(curr_name_prefix)]:
            raise RuntimeError

        # Build the convolutional layer
        conv_layer = nn.Conv2d(input_size, trial.params["{}_output".format(curr_name_prefix)],
                               kernel_size=trial.params["{}_kernel_size".format(curr_name_prefix)],
                               stride=trial.params["{}_stride".format(curr_name_prefix)],
                               padding=trial.params["{}_padding".format(curr_name_prefix)])
        
        c_layers.append(conv_layer)

        # Build the ReLU
        c_layers.append(nn.ReLU(inplace=True))

        # Build the maxpool layer if it should be present
        if trial.params["{}_contains_maxpool".format(curr_name_prefix)]:
            maxpool_layer = nn.MaxPool2d(kernel_size=trial.params["{}_maxpool_kernel_size".format(curr_name_prefix)],
                                         stride=trial.params["{}_maxpool_stride".format(curr_name_prefix)])

            c_layers.append(maxpool_layer)

        # Build the dropout layer if it should be present
        if trial.params["{}_contains_dropout".format(curr_name_prefix)]:
            dropout_layer = nn.Dropout(p=trial.params["{}_dropout_rate".format(curr_name_prefix)])

            c_layers.append(dropout_layer)

    return nn.Sequential(*c_layers)


def build_l_layers(trial: Trial, first_input_size: int) -> nn.Sequential:
    """
    Builds fully-connected layers based on the trial parameters.

    @param trial: Trial
    @param first_input_size: int, computed input size of first fully-connected layer

    @return: nn.Sequential, built fully-connected layers
    """

    l_layers = []

    for i in range(trial.params["l_layers"]):
        # Create key prefixes that will be used to fetch parameters of the trial
        curr_name_prefix = "l_layers_{}_layer_{}".format(trial.params["l_layers"], i + 1)
        prev_name_prefix = "l_layers_{}_layer_{}".format(trial.params["l_layers"], i)

        # Build the linear layer
        input_size = first_input_size if i == 0 else trial.params["{}_output".format(prev_name_prefix)]
        
        # Reject trials which would build a network that is gradually expanding its fully-connected layers
        if input_size <= trial.params["{}_output".format(curr_name_prefix)]:
            raise RuntimeError
        
        l_layers.append(nn.Linear(input_size, trial.params["{}_output".format(curr_name_prefix)]))

        # Build the ReLU
        if i != trial.params["l_layers"] - 1:
            l_layers.append(nn.ReLU(inplace=True))

    return nn.Sequential(*l_layers)
