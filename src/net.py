import torch.nn as nn
import torch
import torch.nn.functional as f

from typing import Tuple


"""
The following classes are modified versions of the classes from this notebook:
    title: Signature Classification Using Siamese (Pytorch)
    authors: RobinReni
    year: 2019
    url: https://www.kaggle.com/code/robinreni/signature-classification-using-siamese-pytorch
    url-date: 2023-05-16
"""


class ContrastiveLoss(torch.nn.Module):
    """
    Class that represents the Contrastive loss function.
    Based on: http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    """

    def __init__(self, margin: float):
        """
        Constructor.

        @param margin: float
        """

        super(ContrastiveLoss, self).__init__()
        self.margin = margin

    def forward(self, output1: torch.Tensor, output2: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        """
        Computes Contrastive loss.

        @param output1: torch.Tensor, first output of the siamese network
        @param output2: torch.Tensor, second output of the siamese network
        @param label: torch.Tensor, label of the samples

        @return: torch.Tensor
        """

        euclidean_distance = f.pairwise_distance(output1, output2)
        loss_part = (1 - label).float() * torch.pow(euclidean_distance, 2).float() + label.float() * torch.pow(
            torch.clamp(self.margin - euclidean_distance, min=0.0), 2).float()

        return torch.mean(loss_part)


class SiameseNetwork(nn.Module):
    """
    Class that represents a Siamese convolutional neural network.
    """

    def __init__(self, cnn: nn.Sequential, fc: nn.Sequential):
        """
        Constructor.

        @param cnn: nn.Sequential, convolutional layers
        @param fc: nn.Sequential, fully-connected layers
        """

        super(SiameseNetwork, self).__init__()
        self.cnn = cnn
        self.fc = fc

    def forward_once(self, input_img: torch.Tensor) -> torch.Tensor:
        """
        Send one image through the network

        @param input_img: torch.Tensor

        @return: torch.Tensor, output of the network
        """

        output = self.cnn(input_img)
        output = output.view(output.size()[0], -1)
        output = self.fc(output)

        return output

    def forward(self, input_img1: torch.Tensor, input_img2: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        """
        Send both images through the network.

        @param input_img1: torch.Tensor
        @param input_img2: torch.Tensor

        @return: tuple, output of the two networks
        """

        output1 = self.forward_once(input_img1)
        output2 = self.forward_once(input_img2)

        return output1, output2
