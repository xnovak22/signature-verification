import os
import csv
import torchvision.transforms as transforms

from PIL import Image
from typing import Tuple, List
from torch.utils.data import Dataset


class SiameseNetworkDataset(Dataset):
    """
    The class represents a dataset used for a siamese neural network -- unlike
    "normal" dataset, it is customized to return two images instead of one.

    It is a modified version of the class from this notebook:
        title: Signature Classification Using Siamese (Pytorch)
        authors: RobinReni
        year: 2019
        url: https://www.kaggle.com/code/robinreni/signature-classification-using-siamese-pytorch
        url-date: 2023-05-16
    """

    def __init__(self, data_dir: str, is_train: bool, image_input_size: int):
        """
        Constructor.

        @param data_dir: str, path to directory with dataset, has to contain
        the index file with paths to images (e.g. test.csv, train.csv, ...)
        @param is_train: bool, indicates wherether to load train or test data
        @param image_input_size: int, dimension to which images will be resized
        """
        self.data_dir = data_dir
        self.variations = self.get_variations(is_train)
        self.image_input_size = image_input_size

    def get_variations(self, is_train: bool) -> List[List[str]]:
        """
        Returns all samples (two images and the label) from the index file.
        
        @param is_train: bool, indicates wherether to load train or test data

        @return: List[List[str]], list of all pairs of images and their label
        """
        
        # Get the path to the index file
        index_file_name = "train.csv" if is_train else "test.csv"
        index_path = os.path.join(self.data_dir, index_file_name)

        # Open the index file and get variations as a list
        with open(index_path, newline='') as variations:
            reader = csv.reader(variations)
            variations_list: List[List[str]] = list(reader)
            
            # Replace path separators with the separator of the current OS
            for i in range(len(variations_list)):
                variations_list[i] = [variations_list[i][0].replace("/", os.sep).replace("\\", os.sep),
                                      variations_list[i][1].replace("/", os.sep).replace("\\", os.sep),
                                      variations_list[i][2]]
                
            return variations_list

    def __getitem__(self, index: int) -> Tuple[Image.Image, Image.Image, int]:
        """
        Gets a sample (image1, image2, label) from the dataset and
        applies preprocessing.

        @param index: int, index of the sample
        @return: tuple[Image.Image, Image.Image, int], two images and their label
        """

        # Get the paths and label of a sample and open the images
        image_variation = self.variations[index]
        img0 = Image.open(image_variation[0])
        img1 = Image.open(image_variation[1])
        
        # Perform preprocessing - convert to grayscale, threshold, resize and convert to tensor
        img0 = img0.convert("L").point(lambda p: 255 if p > 242 else p)
        img1 = img1.convert("L").point(lambda p: 255 if p > 242 else p)

        transform = transforms.Compose([transforms.Resize((self.image_input_size,
                                                           self.image_input_size)),
                                        transforms.ToTensor()])

        img0 = transform(img0)
        img1 = transform(img1)
        
        return img0, img1, int(image_variation[2])

    def __len__(self) -> int:
        """
        Gets count of all samples from the dataset.

        @return: int, count of all samples from the dataset
        """

        return len(self.variations)
