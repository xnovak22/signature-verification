from optuna import Trial
from typing import Tuple, List


def suggest_params_general(trial: Trial) -> None:
    """
    Suggest parameters of the optimizer, the loss function,
    the learning and of the network itself.

    @param trial: Trial

    @return: None
    """

    # Suggesting the parameters of learning
    trial.suggest_int("epochs", 6, 80)
    trial.suggest_int("batch_size", 12, 80, step=4)

    # Suggesting the parameters of the loss function
    trial.suggest_float("margin", 1.8, 2.35)

    # Suggesting the parameters of the optimizer
    trial.suggest_float("lr", 1e-4, 1e-3)
    trial.suggest_float("weight_decay", 1e-4, 1e-3)
    trial.suggest_float("beta1", 0.68, 0.9)
    trial.suggest_float("beta2", 0.78, 0.98)

    # Suggesting the parameters of the network itself
    trial.suggest_int("c_layers", 3, 4)
    trial.suggest_int("l_layers", 3, 4)

    suggest_c_layers(trial)
    suggest_l_layers(trial)


def suggest_c_layer(trial: Trial, n: int, kernel_size: Tuple[int, int], stride: Tuple[int, int],
                    padding: Tuple[int, int], outputs: List[int]) -> None:
    """
    Suggest a convolutional layer.

    @param trial: Trial
    @param n: int, the current number of layer
    @param kernel_size: Tuple[int, int], the smallest and the largest kernel size
    @param stride: Tuple[int, int], the smallest and the largest stride
    @param padding: Tuple[int, int], the smallest and the largest padding
    @param outputs: List[int], the possible output sizes

    @return: None
    """

    # Prefix of the key used for saving the suggestions
    name_prefix = "c_layers_{}_layer_{}".format(trial.params["c_layers"], n)

    # Create the suggestions
    trial.suggest_int("{}_kernel_size".format(name_prefix), kernel_size[0], kernel_size[1], step=2)
    trial.suggest_int("{}_stride".format(name_prefix), stride[0], stride[1])
    trial.suggest_int("{}_padding".format(name_prefix), padding[0], padding[1])
    trial.suggest_categorical("{}_output".format(name_prefix), outputs)


def suggest_maxpool(trial: Trial, n: int) -> None:
    """
    Suggest a maxpooling layer.

    @param trial: Trial
    @param n: int, the current number of layer

    @return: None
    """

    # Prefix of the key used for saving the suggestions
    name_prefix = "c_layers_{}_layer_{}".format(trial.params["c_layers"], n)

    # Create the suggestions.
    # Following suggestions contain multiple identical values to increase probability of choosing some of them.
    # For example, there is now 75% chance that a maxpooling layer will be suggested if the values
    # are sampled randomly
    contains_maxpool = trial.suggest_categorical(
        "{}_contains_maxpool".format(name_prefix), [True, True, True, False])

    if contains_maxpool:
        trial.suggest_categorical("{}_maxpool_kernel_size".format(name_prefix),
                                  [3, 3, 5])
        trial.suggest_categorical("{}_maxpool_stride".format(name_prefix),
                                  [1, 1, 1, 2, 2, 3])


def suggest_dropout(trial: Trial, n: int) -> None:
    """
    Suggest a dropout layer.

    @param trial: Trial
    @param n: the current number of layer

    @return: None
    """

    # Prefix of the key used for saving the suggestions
    name_prefix = "c_layers_{}_layer_{}".format(trial.params["c_layers"], n)

    # Create suggestions.
    # The same note about identical values (see suggest_maxpool function)
    contains_dropout = trial.suggest_categorical(
        "{}_contains_dropout".format(name_prefix), [True, True, False])

    if contains_dropout:
        trial.suggest_float("{}_dropout_rate".format(name_prefix), 0.4, 0.9, step=0.05)


def suggest_c_layers(trial: Trial) -> None:
    """
    Suggest all convolutional layers.

    @param trial: Trial

    @return: None
    """

    # Search space for convolutional layers based on their overall count
    if trial.params["c_layers"] == 3:
        kernel_sizes = [(3, 7), (3, 11), (3, 13)]
        strides = [(1, 2), (1, 3), (1, 3)]
        paddings = [(1, 3), (1, 5), (1, 6)]
        outputs = [[16, 32, 48, 64, 96, 128],
                   [128, 192, 256, 320, 384, 448],
                   [256, 384, 448, 512, 576, 640]]

    else:
        kernel_sizes = [(3, 5), (3, 7), (5, 13), (5, 13)]
        strides = [(1, 2), (1, 2), (1, 3), (1, 3)]
        paddings = [(1, 2), (1, 3), (1, 6), (1, 6)]
        outputs = [[8, 16, 32, 48, 64, 96],
                   [48, 64, 96, 128, 192, 224],
                   [128, 160, 192, 256, 320],
                   [192, 256, 384, 512, 640]]

    # Suggest each convolutional layer, maxpooling and dropout
    for i in range(0, trial.params["c_layers"]):
        suggest_c_layer(trial, n=i + 1, kernel_size=kernel_sizes[i],
                        stride=strides[i], padding=paddings[i],
                        outputs=outputs[i])

        suggest_maxpool(trial, n=i + 1)

        suggest_dropout(trial, n=i + 1)


def suggest_l_layers(trial: Trial) -> None:
    """
    Sugges all fully-connected layers.

    @param trial: Trial

    @return: None
    """

    # Search space for fully-connected layers based on their overall count
    if trial.params["l_layers"] == 3:
        outputs = [[256, 512, 768, 1024, 1536, 2048, 3072, 4096, 6144],
                   [24, 32, 64, 96, 128, 256, 384, 512, 768, 1024, 1536],
                   [2, 4, 8, 16, 32, 64, 128, 192, 256]]
    else:
        outputs = [[768, 1024, 2048, 3072, 4096, 6144, 8192],
                   [512, 768, 1024, 1536, 2048, 3072, 4096],
                   [96, 128, 256, 384, 512, 768, 1024, 1536],
                   [2, 4, 8, 16, 32, 64, 128, 192, 256]]

    # Suggest each fully-connected layer
    for i in range(0, trial.params["l_layers"]):
        trial.suggest_categorical(
            "l_layers_{}_layer_{}_output".format(trial.params["l_layers"], i + 1), outputs[i])
