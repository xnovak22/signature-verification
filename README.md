# Signature Verification
This repository contains the code and data for training, tuning and evaluating siamese convolutional neural networks used in my Signature Verification bachelor thesis at Masaryk University, Faculty of Informatics in Brno.<br />
The work is being created under the supervision of RNDr. Zuzana Nevěřilová, Ph.D. and in cooperation of Natural Language Processing Laboratory at Masaryk University and Konica Minolta.
The link to the thesis will appear here as soon as the work is submitted.

# Contributions
I thank Metacentrum association for providing the much needed computational power. <br />
All the sources are properly mentioned in the thesis or in the code.
